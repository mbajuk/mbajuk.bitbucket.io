
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

wikiGet();

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  ehrId = "";

  // TODO: Potrebno implementirati
  if(stPacienta == 1){
	  var ime = "Veliki";
	  var priimek = "Športnik";
	  var datumRojstva = "1989-03-12T05:19"
	  var sistolicniTlak = 90 + parseInt(Math.random()*10);
	  var diastolicniTlak = 60 + parseInt(Math.random()*5);
	  var srcniUtrip = 40 + parseInt(Math.random()*30);
	  var starost = 2017 - 1989;
	  sistolicniTlak = sistolicniTlak.toString();
	  diastolicniTlak = diastolicniTlak.toString();
	  srcniUtrip = srcniUtrip.toString();
	  starost = starost.toString();
	  ehrId = novEhrZapis(ime,priimek,starost,sistolicniTlak,diastolicniTlak,srcniUtrip, datumRojstva, stPacienta);
  }
   if(stPacienta == 2){
	  var ime = "Malo";
	  var priimek = "Stara";
	  var datumRojstva = "1946-09-27T11:25"
	  var sistolicniTlak = 120 + parseInt(Math.random()*12);
	  var diastolicniTlak = 80 + parseInt(Math.random()*10);
	  var srcniUtrip = 90 + parseInt(Math.random()*20);
	  var starost = 2017 - 1946;
	  sistolicniTlak = sistolicniTlak.toString();
	  diastolicniTlak = diastolicniTlak.toString();
	  srcniUtrip = srcniUtrip.toString();
	  starost = starost.toString();
	  ehrId = novEhrZapis(ime,priimek,starost,sistolicniTlak,diastolicniTlak,srcniUtrip, datumRojstva, stPacienta);
  }
   if(stPacienta == 3){
	  var ime = "Sproščenko";
	  var priimek = "Sproščeni";
	  var datumRojstva = "1993-09-27T11:25"
	  var sistolicniTlak = 70 + parseInt(Math.random()*10);
	  var diastolicniTlak = 40 + parseInt(Math.random()*5);
	  var srcniUtrip = 20 + parseInt(Math.random()*10);
	  var starost = 2017 - 1993;
	  sistolicniTlak = sistolicniTlak.toString();
	  diastolicniTlak = diastolicniTlak.toString();
	  srcniUtrip = srcniUtrip.toString();
	  starost = starost.toString();
	  ehrId = novEhrZapis(ime,priimek,starost,sistolicniTlak,diastolicniTlak,srcniUtrip, datumRojstva, stPacienta);
  }
  document.getElementById("ustvarjeno").innerHTML = "Podatki uspešno generirani!"
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
function prikazPodatkov(){
    var sistolicniTlak;
	var diastolicniTlak;
	var srcniUtrip;
	var party;
	var ogrozenost = 0;
	var stanje = "Neogrožen";
	sessionId = getSessionId();

	var ehrId = $("#meritveVitalnihZnakovEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#obvestiloPrikaza").html("<span <div class='alert alert-danger'>Prišlo je do napake. Vaš zapis ne vsebuje dovolj podatkov.</div></span>");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				party = data.party;
				$("#obvestiloPrikaza").html("");
		  
		  var i = 0;
		  var dolzina = party.partyAdditionalInfo.length;
		  var pravilenZapis = 0;
		  for(var j = 0; j<dolzina;j++){
		  	if(party.partyAdditionalInfo[j].key == "sistolicniTlak" || party.partyAdditionalInfo[j].key == "diastolicniTlak"
		  	|| party.partyAdditionalInfo[j].key == "srcniUtrip" || party.partyAdditionalInfo[j].key == "starost"){
		  		pravilenZapis++;
		  	}
		  }
		  if(pravilenZapis == 4){
		  	document.getElementById("ime").innerHTML = "(" + party.firstNames + " " + party.lastNames + ")";
		  while(party.partyAdditionalInfo[i].key != "sistolicniTlak"){
			  i++;
		  }
		  sistolicniTlak = party.partyAdditionalInfo[i].value;
		  i=0;
		  while(party.partyAdditionalInfo[i].key != "diastolicniTlak"){
			  i++;
		  }
		  diastolicniTlak = party.partyAdditionalInfo[i].value;
		  i=0;
		  while(party.partyAdditionalInfo[i].key != "srcniUtrip"){
			  i++;
		  }
		  srcniUtrip = party.partyAdditionalInfo[i].value;
		   i=0;
		  while(party.partyAdditionalInfo[i].key != "starost"){
			  i++;
		  }
		  starost = party.partyAdditionalInfo[i].value;
		  document.getElementById("imeVrednost").innerHTML = party.firstNames;
		  document.getElementById("priimekVrednost").innerHTML = party.lastNames;
		  document.getElementById("starostVrednost").innerHTML = starost;
		  document.getElementById("sistolicniVrednost").innerHTML = sistolicniTlak;
		  document.getElementById("diastolicniVrednost").innerHTML = diastolicniTlak;
		  document.getElementById("utripVrednost").innerHTML = srcniUtrip;
		  if(sistolicniTlak >= 90 && sistolicniTlak <= 120 && diastolicniTlak >= 60 && diastolicniTlak <= 80 && srcniUtrip >= 40 && srcniUtrip<=100){
			  config1.circleColor = "rgb(0,176,80)";
			  config1.waveColor = "rgb(93,255,166)";
			  config1.textColor = "rgb(0,178,80)";
			  config1.waveTextColor = "white";
			  ogrozenost = 0;
			  stanje = "Neogrožen";
		  }
		  if(!(sistolicniTlak >= 90 && sistolicniTlak <= 120)){
			   ogrozenost += Math.min(Math.abs(90-parseInt(sistolicniTlak)),Math.abs(parseInt(sistolicniTlak)-120));
		  }
		  if(!(diastolicniTlak >= 60 && diastolicniTlak <= 80)){
			  ogrozenost += Math.min(Math.abs(60-parseInt(diastolicniTlak)),Math.abs(parseInt(diastolicniTlak)-80));
		  }
		  if(!(srcniUtrip >= 40 && srcniUtrip<=100)){
			  ogrozenost += Math.min(Math.abs(40-parseInt(srcniUtrip)),Math.abs(parseInt(srcniUtrip)-100));
		  }
			ogrozenost *= (1.0 + parseInt(starost) / 100.0);
			ogrozenost = parseInt(ogrozenost);
		  if(ogrozenost <= 5){
			  config1.circleColor = "rgb(0,176,80)";
			  config1.waveColor = "rgb(93,255,166)";
			  config1.textColor = "rgb(0,178,80)";
			  config1.waveTextColor = "white";
			  stanje = "Neogrožen";
			  document.getElementById("ogrozenost").style = "color:rgb(0,176,80)";
			  document.getElementById("podrobnostiDiagnostike").innerHTML = "<strong>" + "Vaši vitalni znaki kažejo na dolgo in zdravo življenje. Kar tako naprej!" + "</strong>";
		  }
		  else if(ogrozenost <=35){
			  config1.circleColor = "rgb(255,152,0)";
			  config1.waveColor = "rgb(255,187,87)";
			  config1.textColor = "rgb(255,152,0)";
			  config1.waveTextColor = "rgb(255,226,183)";
			  stanje = "Tvegan";
			   document.getElementById("ogrozenost").style = "color:rgb(255,152,0)";
			    document.getElementById("podrobnostiDiagnostike").innerHTML = "<strong>" + "Vaši vitalni znaki niso najboljši. Vaše srce je v precejšnji nevarnosti. Predlagamo čimprejšnji obisk zdravnika." + "</strong>";
		  }
		  else if(ogrozenost <= 60){
			  config1.circleColor = "rgb(255,51,51)";
			  config1.textColor = "#FF4444";
			  config1.waveTextColor = "#FFAAAA";
			  config1.waveColor = "#FF7777";
			  stanje = "Ogrožen";
			   document.getElementById("ogrozenost").style = "color:rgb(255,51,51)";
			    document.getElementById("podrobnostiDiagnostike").innerHTML = "<strong>" + "Živite zelo tvegano. Vaše življenje je v nevarnosti. Ukrepajte preden bo prepozno." + "</strong>";
		  }
		  else{
			  if(ogrozenost > 100){
				  ogrozenost = 100;
			  }
			  config1.circleColor = "rgb(117,1,1)";
			  config1.textColor = "#FF7777";
			  config1.waveTextColor = "rgb(117,1,1)";
			  config1.waveColor = "black";
			  stanje = "Visoko ogrožen";
			   document.getElementById("ogrozenost").style = "color:rgb(117,1,1)";
			    document.getElementById("podrobnostiDiagnostike").innerHTML = "<strong>" + "Vaši vitalni znaki ne obetajo nič dobrega. Preostane vam še par ur življenja. Predlagamo vam, da jih izkoristite." + "</strong>";
		  }
		 $("#podrobnaDiagnostikaDropDown").val("Nasvet");
		 document.getElementById("podrobnaDiagnostika").innerHTML = "<strong>" + "Nasvet" + "</strong>";
		  document.getElementById("ogrozenost").innerHTML = "<strong>" + stanje + "</strong>";
		  $("#fillgauge1").empty();
		    gauge1 = loadLiquidFillGauge("fillgauge1", ogrozenost, config1);
		  }
		  else{
		  		$("#obvestiloPrikaza").html("<span <div class='alert alert-danger'>Prišlo je do napake. Vaš zapis ne vsebuje dovolj podatkov.</div></span>");
		  }
			},
			error: function(err) {
				$("#obvestiloPrikaza").html("<span <div class='alert alert-danger'>Prišlo je do napake. Vaš zapis ne vsebuje dovolj podatkov.</div></span>");
			}
		});
	}
	
}
function vneseniPodatki(){
	var ime = $("#meritevIme").val();
	var priimek = $("#meritevPriimek").val();
	var starost = $("#meritevStarost").val();
	var sistolicniTlak = $("#meritevSistolicniTlak").val();
	var diastolicniTlak = $("#meritevDiastolicniTlak").val();
	var srcniUtrip = $("#meritevSrcniUtrip").val();
	var letoRojstva = (2017 - starost) +"-01-01T00:00";
	var jeGenerator = 0;
	var ehr = novEhrZapis(ime, priimek, starost, sistolicniTlak, diastolicniTlak, srcniUtrip, letoRojstva, 0);
}
function novEhrZapis(ime, priimek, starost, sistolicniTlak, diastolicniTlak, srcniUtrip, letoRojstva, jeGenerator){
    sessionId = getSessionId();

	if (!ime || !priimek || !starost || !sistolicniTlak || !diastolicniTlak || !srcniUtrip || ime.trim().length == 0 ||
      priimek.trim().length == 0 || starost.trim().length == 0 || sistolicniTlak.trim().length == 0 || diastolicniTlak.trim().length == 0 || srcniUtrip.trim().length == 0) {
		$("#obvestiloPrijave").html("<span <div class='alert alert-warning'>Niste izpolnili vseh polj.</div></span>");
		$("#obvestiloPrijave2").html("<span></span>");
		return 0;
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: letoRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId},{key: "sistolicniTlak", value: sistolicniTlak},{key: "diastolicniTlak", value:diastolicniTlak},{key: "srcniUtrip", value: srcniUtrip}, {key: "starost", value:starost}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
							if(jeGenerator == 0){
							$("#obvestiloPrijave").html("<span <div class='alert alert-success'>Uspešno ste vnesli podatke.</div></span>");
							$("#obvestiloPrijave2").html("<span <div class='row'><p style='color:white'>Vaš EHR ID: "+ehrId+"</div></span>");
		                    $("#meritveVitalnihZnakovEHRid").val(ehrId);
							}
							else{
								if(jeGenerator == 1){
									document.getElementById("prvi").innerHTML = ehrId;
									document.getElementById("vPrvi").value = ehrId;
								}
								else if(jeGenerator == 2){
									document.getElementById("drugi").innerHTML = ehrId;
									document.getElementById("vDrugi").value = ehrId;
								}
								else if(jeGenerator == 3){
									document.getElementById("tretji").innerHTML = ehrId;
									document.getElementById("vTretji").value = ehrId;
								}
								document.getElementById("preberiObstojeciEHR").value = "";
							}
		                }
		            },
		            error: function(err) {
		            	 $("#obvestiloPrijave").html("<span <div class='alert alert-danger'>Prišlo je do napake.</div></span>");
						 $("#obvestiloPrijave2").html("<span></span>");
						 return 0;
		            }
		        });
		    }
		});
	}
}
function wikiGet() {
	$.ajax({
		type: "GET",
		url: "https://sl.wikipedia.org/w/api.php?action=parse&format=json&prop=text&section=1&page=Srčno-žilna_bolezen&callback=?",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function(data) {
			var dat = data.parse.text["*"];
			var res = dat.split("<a"); 
			res[3] = res[3].replace("//upload.wikimedia.org/wikipedia/commons/thumb/8/85/Inflammatory_heart_diseases_world_map_-_DALY_-_WHO2004.svg/220px-Inflammatory_heart_diseases_world_map_-_DALY_-_WHO2004.svg.png","knjiznice/wiki2.png");
			res[3] = res[3].replace("width=\"220\" height=\"97\"","width=\"100%\" height=\"100%\" style=\"text-align:center\"");
			res[3] = res[3].replace("class=\"thumbcaption\"","class=\"thumbcaption\" style=\"color:white\"");
			res[5] = res[5].replace("href=\"/wiki/Nezmo%C5%BEnosti_prilagojena_leta_%C5%BEivljenja\"", "href=\"https://sl.wikipedia.org/wiki/Nezmo%C5%BEnosti_prilagojena_leta_%C5%BEivljenja\"");
			res[6] = res[6].replace("<p>Obstaja več srčno-žilnih bolezni, ki prizadenejo krvne žile; gre za t. i. žilne bolezni oziroma bolezni ožilja:</p>", "");
			res[6] = res[6].replace("\n<ul>\n<li>", "");
			 $("#wikiPodatek").html("<a"+res[3]+"<a"+res[4] + "<a" +res[5] + "<a" +res[6]);
		}
	})
}
function expand(content,heading,text) {
		
			var expandContent = document.getElementById(content);
			var contentClass = document.getElementsByClassName('expander-invisible');
			
			
			if (expandContent.className != 'expander-visible'){
			
				expandContent.setAttribute('class', 'expander-visible'); //Show Div
			
			} else {
			
				expandContent.setAttribute('class', 'expander-invisible'); //Hide Div
			
			}
			
  var expandHeading = document.getElementById(heading);
			var headingClass = document.getElementsByClassName('expander-content-collapsed');
			
			if (expandHeading.className != 'expander-content-expanded'){
			
				expandHeading.setAttribute('class', 'expander-content-expanded'); //Show Div
			
			} else {
			
				expandHeading.setAttribute('class', 'expander-content-collapsed'); //Hide Div
			
			}
  
  var expandText = document.getElementById(text);
			var textClass = document.getElementsByClassName('expander-inactive');
			
			if (expandText.className != 'expander-active'){
			
				expandText.setAttribute('class', 'expander-active'); //Show Div
			
			} else {
			
				expandText.setAttribute('class', 'expander-inactive'); //Hide Div
			
			}
			
		}
$(document).ready(function() {
		$('#preberiObstojeciEHR').change(function() {
    var id = $(this).val();
    $("#meritveVitalnihZnakovEHRid").val(id);
  });
  $('#podrobnaDiagnostikaDropDown').change(function() {
    var tipDiagnostike = $(this).val();
	document.getElementById("podrobnaDiagnostika").innerHTML = "<strong>" + tipDiagnostike + "</strong>";
	if(tipDiagnostike == "Nasvet"){
		var ogrozenost = $("#ogrozenost").text();
		if(ogrozenost == "Neogrožen"){
			 document.getElementById("podrobnostiDiagnostike").innerHTML = "<strong>" + "Vaši vitalni znaki kažejo na dolgo in zdravo življenje. Kar tako naprej!" + "</strong>";
		}
		else if(ogrozenost == "Tvegan"){
			document.getElementById("podrobnostiDiagnostike").innerHTML = "<strong>" + "Vaši vitalni znaki niso najboljši. Vaše srce je v precejšnji nevarnosti. Predlagamo čimprejšnji obisk zdravnika." + "</strong>";
		}
		else if(ogrozenost == "Ogrožen"){
			document.getElementById("podrobnostiDiagnostike").innerHTML = "<strong>" + "Živite zelo tvegano. Vaše življenje je v nevarnosti. Ukrepajte preden bo prepozno." + "</strong>";
		}
		else if(ogrozenost == "Visoko ogrožen"){
			document.getElementById("podrobnostiDiagnostike").innerHTML = "<strong>" + "Vaši vitalni znaki ne obetajo nič dobrega. Preostane vam še par ur življenja. Predlagamo vam, da jih izkoristite." + "</strong>";
		}
	}
	else if(tipDiagnostike=="Srčni utrip"){
		var utrip =  $("#utripVrednost").text();
		utrip = parseInt(utrip);
		if(utrip > 100){
			var previsok = utrip-100;
			document.getElementById("podrobnostiDiagnostike").innerHTML = "<strong>" + "Vaš srčni utrip je previsok (za "+previsok+" od zgornje meje). To je lahko posledica različnih bolezni, med katere sodijo slabokrvnost, bolezni ščitnice in druge. Normalne vrednosti srčnega utripa v mirovanju lahko segajo vse od 40 do 100 udarcev na minuto. Hitrost srčnega utripa lahko niha glede na vašo fizično pripravljenost – bolj ko ste telesno dejavni, nižji bo vaš srčni utrip v času mirovanja. " + "</strong>";
		}
		else if(utrip < 40){
			var prenizek = 40 - utrip;
			document.getElementById("podrobnostiDiagnostike").innerHTML = "<strong>" + "Vaš srčni utrip je prenizek (za "+prenizek+" od spodnje meje). To je lahko znak, da vam srce slabi. Normalne vrednosti srčnega utripa v mirovanju lahko segajo vse od 40 do 100 udarcev na minuto. Hitrost srčnega utripa lahko niha glede na vašo fizično pripravljenost – bolj ko ste telesno dejavni, nižji bo vaš srčni utrip v času mirovanja. " + "</strong>";
		}
		else{
			document.getElementById("podrobnostiDiagnostike").innerHTML = "<strong>" + "Vaš srčni utrip je v mejah normale. Kar tako naprej! Normalne vrednosti srčnega utripa v mirovanju lahko segajo vse od 40 do 100 udarcev na minuto. Hitrost srčnega utripa lahko niha glede na vašo fizično pripravljenost – bolj ko ste telesno dejavni, nižji bo vaš srčni utrip v času mirovanja. " + "</strong>";
		}
	}
	else if(tipDiagnostike=="Diastolični tlak"){
		var diastolicni = $("#diastolicniVrednost").text();
		diastolicni = parseInt(diastolicni);
		if(diastolicni > 80){
			var previsok = diastolicni-80;
			document.getElementById("podrobnostiDiagnostike").innerHTML = "<strong>" + "Vaš diastolični (spodnji) krvni tlak je previsok (za "+previsok+" od zgornje meje). Normalne vrednosti segajo vse od 60 do 80 mmHg. Možni vzroki so prevelika telesna teža, čezmerno uživanje soli, čezmerno pitje alkoholnih pijač, premajhna telesna dejavnost, izpostavljenost škodljivim stresom." + "</strong>";
		}
		else if(diastolicni < 60){
			var prenizek = 60-diastolicni;
			document.getElementById("podrobnostiDiagnostike").innerHTML = "<strong>" + "Vaš diastolični (spodnji) krvni tlak je prenizek (za "+prenizek+" od spodnje meje). Normalne vrednosti segajo vse od 60 do 80 mmHg. Nizek krvni tlak je običajno manj škodljiv kot visok. Kljub temu pa je, v kombinaciji s še nekaterimi drugimi simptomi, lahko znanilec nekaterih drugih bolezni. Zdravljenje znižanega krvnega tlaka je odvisno od vzroka. Bolnikom zdravniki običajno odsvetujejo dolgotrajno ležanje, priporočajo pa počasno vstajanje in zmerno telesno aktivnost ter spanje z višjim vzglavjem." + "</strong>";
		}
		else{
			document.getElementById("podrobnostiDiagnostike").innerHTML = "<strong>" + "Vaš diastolični (spodnji) krvni tlak je v mejah normale. Kar tako naprej! Normalne vrednosti segajo vse od 60 do 80 mmHg." + "</strong>";
		}
		
	}
	else if(tipDiagnostike=="Sistolični tlak"){
		var sistolicni = $("#sistolicniVrednost").text();
		sistolicni = parseInt(sistolicni);
		if(sistolicni > 120){
			var previsok = sistolicni-120;
			document.getElementById("podrobnostiDiagnostike").innerHTML = "<strong>" + "Vaš sistolični (zgornji) krvni tlak je previsok (za "+previsok+" od zgornje meje). Normalne vrednosti segajo vse od 90 do 120 mmHg. Možni vzroki so prevelika telesna teža, čezmerno uživanje soli, čezmerno pitje alkoholnih pijač, premajhna telesna dejavnost, izpostavljenost škodljivim stresom." + "</strong>";
		}
		else if(sistolicni < 90){
			var prenizek = 90-sistolicni;
						document.getElementById("podrobnostiDiagnostike").innerHTML = "<strong>" + "Vaš sistolični (zgornji) krvni tlak je prenizek (za "+prenizek+" od spodnje meje). Normalne vrednosti segajo vse od 90 do 120 mmHg. Nizek krvni tlak je običajno manj škodljiv kot visok. Kljub temu pa je, v kombinaciji s še nekaterimi drugimi simptomi, lahko znanilec nekaterih drugih bolezni. Zdravljenje znižanega krvnega tlaka je odvisno od vzroka. Bolnikom zdravniki običajno odsvetujejo dolgotrajno ležanje, priporočajo pa počasno vstajanje in zmerno telesno aktivnost ter spanje z višjim vzglavjem." + "</strong>";
		}
		else{
			document.getElementById("podrobnostiDiagnostike").innerHTML = "<strong>" + "Vaš sistolični (zgornji) krvni tlak je v mejah normale. Kar tako naprej! Normalne vrednosti segajo vse od 90 do 120 mmHg." + "</strong>";
		}
	}
  });
  });